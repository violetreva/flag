This is a series of flags for your inspiration. Licensed under CC-by-SA 4.0+

# Symbolism

Red = socialism, black = absence of tyranny, lavender = lesbians, purple = feminism.

# How to View / Open

.PNG is a lossless format that is also frugal on disk space. Most things can open it.

.XCF is a format openable in the GIMP image editor: http://www.gimp.org/
